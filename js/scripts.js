


document.querySelector('.burger').addEventListener('click', function(){
	document.querySelector('nav').classList.toggle('open');
})

// ANIMATED LOGO //

var textWrapper = document.querySelector('.ml1 .letters');
textWrapper.innerHTML = textWrapper.textContent.replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>");

anime.timeline({loop: true})
  .add({
    targets: '.ml1 .letter',
    scale: [0.3,1],
    opacity: [0,1],
    translateZ: 0,
    easing: "easeOutExpo",
    duration: 900,
    delay: function(el, i) {
      return 70 * (i+1)
    }
  }).add({
    targets: '.ml1 .line',
    scaleX: [0,1],
    opacity: [0.5,1],
    easing: "easeOutExpo",
    duration: 4000,
    offset: '-=875',
    delay: function(el, i, l) {
      return 80 * (l - i);
    }
  }).add({
    targets: '.ml1',
    opacity: 0,
    duration: 2000,
    easing: "easeOutExpo",
    delay: 4000
  });


// SLICK MAIN //

$('.myCarousel').slick({
	autoplay: true,
	autoplaySpeed: 2500,
	dots: true,
	prevArrow: false,
	nextArrow: false
});

// SLICK EXPLORE //

$('.topCarousel').slick({
 	infinite: true,
  	slidesToShow: 3,
  	slidesToScroll: 3,
  	autoplay: true,
  	autoplaySpeed: 3000,
  	dots: true,
  	prevArrow: false,
	nextArrow: false,
	responsive: [
		{
			breakpoint: 574,
			settings: {
				slidesToShow: 2,
  				slidesToScroll: 2
			}
		}
	]
});



// SCROLL POP-OUT BOX //

$(window).scroll( function() {
	var pageTop = $(window).scrollTop();
	var viewHeight = $(window).height();

	var element = $('.pop-hero');
	console.log()
	element.each(function() {
		var elementTop = $(this).offset().top;

		if(elementTop <= pageTop + (viewHeight * 0.8) ){

			$(this).addClass('active');

			console.log($(this));

		} else{
			$(this).removeClass('active');
		}
	});
});

// SCROLL COMPRESS HEADER //

$(window).scroll( function() {
	var pageTop = $(window).scrollTop();
	var viewHeight = $(window).height();

	var element = $('#first-section');
	element.each(function() {
		var elementTop = $(this).offset().top;
		console.log(elementTop)
		console.log(pageTop)

		if(pageTop > elementTop ){

			$('header').css({height: '40px'})

		} else{
			$('header').css({height: '50px'})
		}
	});
});

// DESTINATIONS FILTER //

filterSelection("all")
function filterSelection(c) {
  var x, i;
  x = document.getElementsByClassName("filterDiv");
  if (c == "all") c = "";
  for (i = 0; i < x.length; i++) {
    removeClass(x[i], "show");
    if (x[i].className.indexOf(c) > -1) addClass(x[i], "show");
  }
}

// show
function addClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {
      element.className += " " + arr2[i];
    }
  }
}

function removeClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1); 
    }
  }
  element.className = arr1.join(" ");
}

if( document.querySelector('.btn') ){
	var btnContainer = document.getElementById("myBtnContainer");
	var btns = btnContainer.getElementsByClassName("btn");
	for (var i = 0; i < btns.length; i++) {
	  btns[i].addEventListener("click", function() {
	    var current = document.getElementsByClassName("active");
	    current[0].className = current[0].className.replace(" active", "");
	    this.className += " active";
	  });
	}
}

// FADE IN //

$(function(){  // $(document).ready shorthand
  $('.monster').fadeIn('slow');
});

$(document).ready(function() {
    
    /* Every time the window is scrolled ... */
    $(window).scroll( function(){
    
        /* Check the location of each desired element */
        $('.hideme').each( function(i){
            
            var bottom_of_object = $(this).position().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            
            /* If the object is completely visible in the window, fade it it */
            if( bottom_of_window > bottom_of_object ){
                
                $(this).animate({'opacity':'1'},500);
                    
            }
            
        }); 
    
    });
    
});
